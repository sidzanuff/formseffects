﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace VortexInfinitum.Forms.Effects
{
    public class ExplodeEffect : ParticleEffect<MovingParticle>
    {
        private const int MIN_VEL = 50;
        private const int MAX_VEL = 100;

        private Random random;
        private Bitmap bitmap;
        private Point offset;

        public ExplodeEffect()
        {
            this.random = new Random((int)DateTime.Now.Ticks);

            MinimumVelocity = MIN_VEL;
            MaximumVelocity = MAX_VEL;
        }

        public Bitmap Bitmap
        {
            get
            {
                return bitmap;
            }
            set
            {
                bitmap = value;
                UpdateOffset();
            }
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            UpdateOffset();
        }

        private void UpdateOffset()
        {
            if (bitmap != null)
            {
                offset = new Point(ClientSize.Width / 2 - bitmap.Width / 2, ClientSize.Height / 2 - bitmap.Height / 2);
            }
        }

        public int MinimumVelocity { get; set; }

        public int MaximumVelocity { get; set; }

        protected override List<MovingParticle> GenerateParticles()
        {
            List<MovingParticle> particles = new ParticleExtractor<MovingParticle>(Bitmap).Extract();

            foreach (MovingParticle particle in particles)
            {
                PointF vel = CalculateVelocity(particle);
                particle.VelocityX = vel.X;
                particle.VelocityY = vel.Y;

                particle.X += offset.X;
                particle.Y += offset.Y;
            }

            return particles;
        }

        protected override void UpdateParticle(EffectTime effectTime, MovingParticle particle)
        {
            particle.X += particle.VelocityX * (float)effectTime.ElapsedEffectTime.TotalMilliseconds / 1000;
            particle.Y += particle.VelocityY * (float)effectTime.ElapsedEffectTime.TotalMilliseconds / 1000;
        }

        protected override bool ShouldDeleteParticle(MovingParticle particle)
        {
            return particle.X < 0 || particle.Y < 0 || particle.X >= ClientSize.Width || particle.Y >= ClientSize.Height;
        }

        private PointF CalculateVelocity(Particle particle)
        {
            float x = Bitmap.Width / 2 - particle.X;
            float y = Bitmap.Height / 2 - particle.Y;

            if (x == 0 && y == 0)
            {
                x = random.Next(0, 2) == 0 ? 1 : -1;
                y = random.Next(0, 2) == 0 ? 1 : -1;
            }

            float length = (float)Math.Sqrt(x * x + y * y);

            float vel = (float)random.NextDouble() * (MaximumVelocity - MinimumVelocity) + MinimumVelocity;

            return new PointF(x / length * vel * -1, y / length * vel * -1);
        }
    }
}