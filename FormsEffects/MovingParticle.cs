﻿namespace VortexInfinitum.Forms.Effects
{
    public class MovingParticle : Particle
    {
        public float VelocityX { get; set; }

        public float VelocityY { get; set; }
    }
}