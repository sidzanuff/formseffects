﻿using System.Collections.Generic;

namespace VortexInfinitum.Forms.Effects
{
    internal class YFirstParticleComparer<T> : IComparer<T> where T : Particle
    {
        public int Compare(T first, T second)
        {
            if (first.Y == second.Y)
            {
                return (int)(first.X - second.X);
            }
            else
            {
                return (int)(first.Y - second.Y);
            }
        }
    }
}