﻿using System.Drawing;
using System.Drawing.Imaging;

namespace VortexInfinitum.Forms.Effects
{
    internal unsafe class FastBitmap : Canvas
    {
        private const PixelFormat PIXEL_FORMAT = PixelFormat.Format24bppRgb;

        private Bitmap bitmap;
        private Rectangle rectangle;
        private int bytesPerPixel;
        private byte* firstPixel;
        private BitmapData bitmapData;

        public FastBitmap(int width, int height)
        {
            bitmap = new Bitmap(width, height, PIXEL_FORMAT);
            rectangle = new Rectangle(0, 0, width, height);
            bytesPerPixel = Bitmap.GetPixelFormatSize(PIXEL_FORMAT) / 8;
        }

        public Bitmap Bitmap { get { return bitmap; } }

        public void Clear(Color color)
        {
            using (Graphics g = Graphics.FromImage(bitmap))
            {
                g.Clear(color);
            }
        }

        public void Lock()
        {
            bitmapData = bitmap.LockBits(rectangle, ImageLockMode.WriteOnly, PIXEL_FORMAT);
            firstPixel = (byte*)bitmapData.Scan0;
        }

        public void DrawParticle(Particle particle)
        {
            if (particle.X < 0 || particle.Y < 0 || particle.X >= bitmap.Width || particle.Y >= bitmap.Height)
            {
                return;
            }

            byte* currentLine = firstPixel + ((int)particle.Y * bitmapData.Stride);
            int i = (int)particle.X * bytesPerPixel;

            currentLine[i] = particle.B;
            currentLine[i + 1] = particle.G;
            currentLine[i + 2] = particle.R;
        }

        public void Unlock()
        {
            bitmap.UnlockBits(bitmapData);
        }
    }
}