﻿using System;

namespace VortexInfinitum.Forms.Effects
{
    public delegate float EaseFunc(float t, float b, float c, float d);

    public static class Easing
    {
        // t - current time
        // b - initial value
        // c - change in value
        // d - duration

        public static float simple(float t, float b, float c, float d)
        {
            return c * (t / d) + b;
        }

        public static float easeInOutCirc(float t, float b, float c, float d)
        {
            if ((t /= d / 2) < 1) return (float)(-c / 2 * (Math.Sqrt(1 - t * t) - 1) + b);
            return (float)(c / 2 * (Math.Sqrt(1 - (t -= 2) * t) + 1) + b);
        }

        public static float easeInOutBack(float t, float b, float c, float d)
        {
            return easeInOutBack(t, b, c, d, 1.70158f);
        }

        public static float easeInOutBack(float t, float b, float c, float d, float s = 1.70158f)
        {
            if ((t /= d / 2) < 1) return c / 2 * (t * t * (((s *= (1.525f)) + 1) * t - s)) + b;
            return c / 2 * ((t -= 2) * t * (((s *= (1.525f)) + 1) * t + s) + 2) + b;
        }

        public static float easeInQuad(float t, float b, float c, float d)
        {
            return c * (t /= d) * t + b;
        }

        public static float easeOutQuad(float t, float b, float c, float d)
        {
            return -c * (t /= d) * (t - 2) + b;
        }

        public static float easeOutBack(float t, float b, float c, float d)
        {
            //if (s == undefined)
            float s = 1.70158f;
            return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b;
        }

        public static float easeOutBounce(float t, float b, float c, float d)
        {
            if ((t /= d) < (1 / 2.75))
            {
                return c * (7.5625f * t * t) + b;
            }
            else if (t < (2 / 2.75))
            {
                return c * (7.5625f * (t -= (1.5f / 2.75f)) * t + .75f) + b;
            }
            else if (t < (2.5 / 2.75))
            {
                return c * (7.5625f * (t -= (2.25f / 2.75f)) * t + .9375f) + b;
            }
            else
            {
                return c * (7.5625f * (t -= (2.625f / 2.75f)) * t + .984375f) + b;
            }
        }

        public static float easeInBounce(float t, float b, float c, float d)
        {
            return c - easeOutBounce(d - t, 0, c, d) + b;
        }

        public static float easeInOutBounce(float t, float b, float c, float d)
        {
            if (t < d / 2) return easeInBounce(t * 2, 0, c, d) * .5f + b;
            return easeOutBounce(t * 2 - d, 0, c, d) * .5f + c * .5f + b;
        }

        public static float easeOutCirc(float t, float b, float c, float d)
        {
            return (float)(c * Math.Sqrt(1 - (t = t / d - 1) * t) + b);
        }

        public static float easeInCirc(float t, float b, float c, float d)
        {
            return (float)(-c * (Math.Sqrt(1 - (t /= d) * t) - 1) + b);
        }
    }
}