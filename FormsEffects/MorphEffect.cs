﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace VortexInfinitum.Forms.Effects
{
    public class MorphEffect : ParticleEffect<DeltaParticle>
    {
        private const int DEFAULT_DURATION = 1500;

        private Bitmap startBitmap;
        private Bitmap endBitmap;

        private Point startOffset;
        private Point endOffset;

        public MorphEffect()
        {
            Duration = DEFAULT_DURATION;

            PositionFunc = Easing.easeInOutBack;
            ColorFunc = Easing.easeInQuad;
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            UpdateOffsets();
        }

        public int Duration { get; set; }

        public EaseFunc PositionFunc { get; set; }

        public EaseFunc ColorFunc { get; set; }

        public Bitmap StartBitmap
        {
            get
            {
                return startBitmap;
            }
            set
            {
                startBitmap = value;
                UpdateOffsets();
            }
        }

        public Bitmap EndBitmap
        {
            get
            {
                return endBitmap;
            }
            set
            {
                endBitmap = value;
                UpdateOffsets();
            }
        }

        private void UpdateOffsets()
        {
            if (startBitmap != null)
            {
                startOffset = new Point(ClientSize.Width / 2 - startBitmap.Width / 2, ClientSize.Height / 2 - startBitmap.Height / 2);

                if (endBitmap != null)
                {
                    endOffset = new Point(startBitmap.Width / 2 - endBitmap.Width / 2, startBitmap.Height / 2 - endBitmap.Height / 2);
                }
            }
        }

        protected override void Update(EffectTime effectTime)
        {
            if (effectTime.TotalEffectTime.TotalMilliseconds > Duration)
            {
                Finish();
            }
            else
            {
                base.Update(effectTime);
            }
        }

        protected override List<DeltaParticle> GenerateParticles()
        {
            List<Particle> startParticles = new ParticleExtractor<Particle>(StartBitmap).Extract();
            List<Particle> endParticles = new ParticleExtractor<Particle>(EndBitmap).Extract(); ;

            int particleCount = Math.Max(startParticles.Count, endParticles.Count);

            MultiplyParticles(startParticles, particleCount);
            MultiplyParticles(endParticles, particleCount);

            List<DeltaParticle> particles = new List<DeltaParticle>(particleCount);

            for (int i = 0; i < particleCount; i++)
            {
                Particle startParticle = startParticles[i];
                Particle endParticle = endParticles[i];

                particles.Add(new DeltaParticle
                {
                    StartX = startParticle.X + startOffset.X,
                    StartY = startParticle.Y + startOffset.Y,

                    StartR = startParticle.R,
                    StartG = startParticle.G,
                    StartB = startParticle.B,

                    DeltaX = endParticle.X + endOffset.X - startParticle.X,
                    DeltaY = endParticle.Y + endOffset.Y - startParticle.Y,

                    DeltaR = endParticle.R - startParticle.R,
                    DeltaG = endParticle.G - startParticle.G,
                    DeltaB = endParticle.B - startParticle.B,
                });
            }

            return particles;
        }

        protected override void UpdateParticle(EffectTime effectTime, DeltaParticle particle)
        {
            particle.X = PositionFunc((float)effectTime.TotalEffectTime.TotalMilliseconds, particle.StartX, particle.DeltaX, Duration);
            particle.Y = PositionFunc((float)effectTime.TotalEffectTime.TotalMilliseconds, particle.StartY, particle.DeltaY, Duration);

            particle.R = (byte)ColorFunc((float)effectTime.TotalEffectTime.TotalMilliseconds, particle.StartR, particle.DeltaR, Duration);
            particle.G = (byte)ColorFunc((float)effectTime.TotalEffectTime.TotalMilliseconds, particle.StartG, particle.DeltaG, Duration);
            particle.B = (byte)ColorFunc((float)effectTime.TotalEffectTime.TotalMilliseconds, particle.StartB, particle.DeltaB, Duration);
        }

        private static void MultiplyParticles<T>(List<T> items, int count) where T : Particle
        {
            int index = 0;

            while (items.Count < count)
            {
                items.Add(items[index]);

                if (++index == items.Count)
                {
                    index = 0;
                }
            }

            items.Sort(new YFirstParticleComparer<T>());
        }
    }
}