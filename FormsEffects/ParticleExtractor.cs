﻿using System.Collections.Generic;
using System.Drawing;

namespace VortexInfinitum.Forms.Effects
{
    internal class ParticleExtractor<T> where T : Particle, new()
    {
        private Bitmap bitmap;

        public ParticleExtractor(Bitmap bitmap)
        {
            this.bitmap = bitmap;
        }

        public List<T> Extract()
        {
            List<T> particles = new List<T>();

            for (int x = 0; x < bitmap.Width; x++)
            {
                for (int y = 0; y < bitmap.Height; y++)
                {
                    Color color = bitmap.GetPixel(x, y);

                    if (color.R > 0 || color.G > 0 || color.B > 0)
                    {
                        T particle = new T();

                        particle.X = x;
                        particle.Y = y;
                        particle.R = color.R;
                        particle.G = color.G;
                        particle.B = color.B;

                        particles.Add(particle);
                    }
                }
            }

            return particles;
        }
    }
}