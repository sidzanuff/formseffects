﻿namespace VortexInfinitum.Forms.Effects
{
    public class DeltaParticle : Particle
    {
        public float StartX { get; set; }

        public float StartY { get; set; }

        public byte StartR { get; set; }

        public byte StartG { get; set; }

        public byte StartB { get; set; }

        public float DeltaX { get; set; }

        public float DeltaY { get; set; }

        public int DeltaR { get; set; }

        public int DeltaG { get; set; }

        public int DeltaB { get; set; }

        public int Duration { get; set; }
    }
}