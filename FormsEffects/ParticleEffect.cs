﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace VortexInfinitum.Forms.Effects
{
    public abstract class ParticleEffect<T> : Effect where T : Particle
    {
        private FastBitmap backBuffer;
        private List<T> particles;

        protected override void OnStart()
        {
            backBuffer = new FastBitmap(ClientSize.Width, ClientSize.Height);
            particles = GenerateParticles();
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);

            backBuffer = new FastBitmap(ClientSize.Width, ClientSize.Height);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (backBuffer != null)
            {
                e.Graphics.DrawImageUnscaled(backBuffer.Bitmap, Point.Empty);
            }
        }

        protected abstract List<T> GenerateParticles();

        protected override void Update(EffectTime effectTime)
        {
            backBuffer.Clear(BackColor);
            backBuffer.Lock();

            for (int i = particles.Count - 1; i >= 0; i--)
            {
                T particle = particles[i];

                UpdateParticle(effectTime, particle);

                if (ShouldDeleteParticle(particle))
                {
                    particles.RemoveAt(i);
                }
                else
                {
                    backBuffer.DrawParticle(particle);
                }
            }

            backBuffer.Unlock();

            if (particles.Count == 0)
            {
                Finish();
            }
        }

        protected abstract void UpdateParticle(EffectTime effectTime, T particle);

        protected virtual bool ShouldDeleteParticle(T particle)
        {
            return false;
        }
    }
}