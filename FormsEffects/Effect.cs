﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace VortexInfinitum.Forms.Effects
{
    public abstract class Effect : Control
    {
        private const int DEFAULT_INTERVAL = 10;

        private Timer timer;
        private DateTime startTime;
        private DateTime lastUpdateTime;

        public event EventHandler Finished;

        public Effect()
        {
            BackColor = Color.Black;
            UpdateInterval = DEFAULT_INTERVAL;

            this.SetStyle(
                ControlStyles.UserPaint |
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.DoubleBuffer, true);

            timer = new Timer();
            timer.Tick += timer_Tick;
        }

        public int UpdateInterval { get; set; }

        public void Start()
        {
            if (timer.Enabled)
            {
                return;
            }

            startTime = DateTime.Now;
            lastUpdateTime = DateTime.Now;

            OnStart();

            timer.Interval = UpdateInterval;
            timer.Start();
        }

        protected abstract void OnStart();

        public void Stop()
        {
            if (!timer.Enabled)
            {
                return;
            }

            timer.Stop();
            Invalidate();
        }

        public bool Running { get { return timer.Enabled; } }

        private void timer_Tick(object sender, EventArgs e)
        {
            EffectTime effectTime = new EffectTime
            {
                ElapsedEffectTime = DateTime.Now - lastUpdateTime,
                TotalEffectTime = DateTime.Now - startTime
            };

            lastUpdateTime = DateTime.Now;

            Update(effectTime);

            Invalidate();
        }

        protected abstract void Update(EffectTime effectTime);

        protected void Finish()
        {
            timer.Stop();

            if (Finished != null) Finished(this, EventArgs.Empty);
        }
    }
}