﻿namespace VortexInfinitum.Forms.Effects
{
    public interface Canvas
    {
        void DrawParticle(Particle particle);
    }
}