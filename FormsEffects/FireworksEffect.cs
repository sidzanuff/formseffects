﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace VortexInfinitum.Forms.Effects
{
    public class FireworksEffect : ParticleEffect<MovingParticle>
    {
        private const int PARTICLE_COUNT = 2000;
        private const int MIN_VEL = 10;
        private const int MAX_VEL = 200;
        private const int GRAVITY = 200;

        private Random random;

        public FireworksEffect()
        {
            this.random = new Random((int)DateTime.Now.Ticks);

            MinimumVelocity = MIN_VEL;
            MaximumVelocity = MAX_VEL;
        }

        public int MinimumVelocity { get; set; }

        public int MaximumVelocity { get; set; }

        protected override List<MovingParticle> GenerateParticles()
        {
            List<MovingParticle> particles = new List<MovingParticle>();

            for (int i = 0; i < PARTICLE_COUNT; i++)
            {
                PointF vel = RandomVelocity();

                particles.Add(new MovingParticle
                    {
                        X = ClientSize.Width / 2,
                        Y = ClientSize.Height / 2,
                        R = (byte)random.Next(100, 255),
                        G = (byte)random.Next(100, 255),
                        B = (byte)random.Next(100, 255),
                        VelocityX = vel.X,
                        VelocityY = vel.Y
                    });
            }

            return particles;
        }

        private PointF RandomVelocity()
        {
            double angle = random.NextDouble() * Math.PI * 2;
            double x = Math.Cos(angle);
            double y = Math.Sin(angle);
            double vel = random.NextDouble() * (MaximumVelocity - MinimumVelocity) + MinimumVelocity;
            return new PointF((float)(x * vel), (float)(y * vel));
        }

        protected override void UpdateParticle(EffectTime effectTime, MovingParticle particle)
        {
            particle.X += particle.VelocityX * (float)effectTime.ElapsedEffectTime.TotalMilliseconds / 1000;
            particle.Y += particle.VelocityY * (float)effectTime.ElapsedEffectTime.TotalMilliseconds / 1000;

            particle.VelocityY += GRAVITY * (float)effectTime.ElapsedEffectTime.TotalMilliseconds / 1000;
        }

        protected override bool ShouldDeleteParticle(MovingParticle particle)
        {
            return particle.X < 0 || particle.Y < 0 || particle.X >= ClientSize.Width || particle.Y >= ClientSize.Height;
        }
    }
}