﻿using System;
using System.Drawing;

namespace VortexInfinitum.Forms.Effects
{
    public class FireworksEffect2 : Effect
    {
        private const int GRAVITY = 200;
        private const int MAX_VEL = 200;
        private const int MIN_VEL = 10;
        private const int PARTICLE_COUNT = 2000;
        private const double TwoPI = Math.PI * 2;

        private static readonly Random random = new Random((int)DateTime.Now.Ticks);

        private Particle[] particles;

        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            e.Graphics.Clear(Color.Black);

            if (particles == null) return;

            for (int i = 0; i < particles.Length; i++)
            {
                if (particles[i].X >= 0 &&
                    particles[i].Y >= 0 &&
                    particles[i].X < ClientSize.Width &&
                    particles[i].Y < ClientSize.Height)
                {
                    e.Graphics.FillEllipse(particles[i].Brush, particles[i].X, particles[i].Y, 4, 4);
                }
            }
        }

        protected override void OnStart()
        {
            particles = new Particle[PARTICLE_COUNT];

            PointF source = new PointF(ClientSize.Width / 2, ClientSize.Height / 2);

            for (int i = 0; i < PARTICLE_COUNT; i++)
            {
                PointF vel = RandomVelocity();

                particles[i] = new Particle
                {
                    X = source.X,
                    Y = source.Y,
                    VelX = vel.X,
                    VelY = vel.Y,
                    Brush = RandomBrush()
                };
            }
        }

        protected override void Update(EffectTime effectTime)
        {
            for (int i = 0; i < particles.Length; i++)
            {
                if (particles[i].X < 0 ||
                    particles[i].Y < 0 ||
                    particles[i].X >= ClientSize.Width ||
                    particles[i].Y >= ClientSize.Height)
                {
                    continue;
                }

                particles[i].X += particles[i].VelX * (float)effectTime.ElapsedEffectTime.TotalMilliseconds / 1000;
                particles[i].Y += particles[i].VelY * (float)effectTime.ElapsedEffectTime.TotalMilliseconds / 1000;
                particles[i].VelY += GRAVITY * (float)effectTime.ElapsedEffectTime.TotalMilliseconds / 1000;
            }
        }

        private Brush RandomBrush()
        {
            return new SolidBrush(
                Color.FromArgb(
                    random.Next(100, 255),
                    random.Next(100, 255),
                    random.Next(100, 255)));
        }

        private PointF RandomVelocity()
        {
            double angle = random.NextDouble() * TwoPI;
            double x = Math.Cos(angle);
            double y = Math.Sin(angle);
            double vel = random.NextDouble() * (MAX_VEL - MIN_VEL) + MIN_VEL;
            return new PointF((float)(x * vel), (float)(y * vel));
        }

        private struct Particle
        {
            public Brush Brush { get; set; }

            public float VelX { get; set; }

            public float VelY { get; set; }

            public float X { get; set; }

            public float Y { get; set; }
        }
    }
}