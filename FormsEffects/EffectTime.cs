﻿using System;

namespace VortexInfinitum.Forms.Effects
{
    public class EffectTime
    {
        public TimeSpan ElapsedEffectTime { get; set; }

        public TimeSpan TotalEffectTime { get; set; }
    }
}