﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace VortexInfinitum.Forms.Effects
{
    public class ConstructEffect : ParticleEffect<DeltaParticle>
    {
        private const int DEFAULT_DURATION = 2000;
        private const int DEFAULT_DURATION_RANGE = 500;

        private Random random;

        public ConstructEffect()
        {
            this.random = new Random((int)DateTime.Now.Ticks);

            Duration = DEFAULT_DURATION;
            TransformFunc = Easing.easeInOutBack;
        }

        public EaseFunc TransformFunc { get; set; }

        public int Duration { get; set; }

        public Bitmap Bitmap { get; set; }

        protected override List<DeltaParticle> GenerateParticles()
        {
            Point offset = new Point(ClientSize.Width / 2 - Bitmap.Width / 2, ClientSize.Height / 2 - Bitmap.Height / 2);
            Point center = new Point(ClientSize.Width / 2, ClientSize.Height / 2);
            int radius = Math.Max(ClientSize.Width / 2, ClientSize.Height / 2);
            List<DeltaParticle> particles = new ParticleExtractor<DeltaParticle>(Bitmap).Extract();

            foreach (DeltaParticle particle in particles)
            {
                Point endPoint = new Point((int)particle.X + offset.X, (int)particle.Y + offset.Y);
                PointF direction = CalculateDirection(endPoint, center);
                PointF startPoint = new PointF(endPoint.X + direction.X * radius, endPoint.Y + direction.Y * radius);

                particle.StartX = startPoint.X;
                particle.StartY = startPoint.Y;

                particle.DeltaX = endPoint.X - startPoint.X;
                particle.DeltaY = endPoint.Y - startPoint.Y;

                particle.Duration = random.Next(Duration - DEFAULT_DURATION_RANGE, Duration);
            }

            return particles;
        }

        private PointF CalculateDirection(Point point, Point center)
        {
            float x = center.X - point.X;
            float y = center.Y - point.Y;

            if (x == 0 && y == 0)
            {
                x = random.Next(0, 2) == 0 ? 1 : -1;
                y = random.Next(0, 2) == 0 ? 1 : -1;
            }

            float length = (float)Math.Sqrt(x * x + y * y);

            return new PointF(x / length * -1, y / length * -1);
        }

        protected override void Update(EffectTime effectTime)
        {
            if (effectTime.TotalEffectTime.TotalMilliseconds > Duration)
            {
                Finish();
            }
            else
            {
                base.Update(effectTime);
            }
        }

        protected override void UpdateParticle(EffectTime effectTime, DeltaParticle particle)
        {
            if (effectTime.TotalEffectTime.TotalMilliseconds > particle.Duration)
            {
                return;
            }

            particle.X = TransformFunc((float)effectTime.TotalEffectTime.TotalMilliseconds, particle.StartX, particle.DeltaX, particle.Duration);
            particle.Y = TransformFunc((float)effectTime.TotalEffectTime.TotalMilliseconds, particle.StartY, particle.DeltaY, particle.Duration);
        }
    }
}