﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using VortexInfinitum.Forms.Effects;

namespace EffectTest
{
    public partial class Form1 : Form
    {
        private List<Effect> effects = new Effect[] {
            new MorphEffect
            {
                StartBitmap = global::EffectTest.Properties.Resources.img2,
                EndBitmap = global::EffectTest.Properties.Resources.Terran_empire_logo
            },
            new ExplodeEffect
            {
                Bitmap = global::EffectTest.Properties.Resources.img2
            },
            new ConstructEffect
            {
                Bitmap = global::EffectTest.Properties.Resources.img2
            },
            new FireworksEffect(),
            new FireworksEffect2()
        }.ToList();

        public Form1()
        {
            InitializeComponent();

            foreach (Effect effect in effects)
            {
                effectsComboBox.Items.Add(effect.GetType().Name);
                effect.Finished += effect_Finished;
                panel1.Controls.Add(effect);
                effect.Dock = DockStyle.Fill;
                effect.Visible = false;
            }

            effectsComboBox.SelectedIndex = 0;
        }

        private void effect_Finished(object sender, EventArgs e)
        {
            startButton.Text = "Start";
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            if (CurrentEffect.Running) StopEffect();
            else StartEffect();
        }

        private void morphRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            effects.ForEach(ef => ef.Stop());
            startButton.Text = "Start";
        }

        private void StartEffect()
        {
            CurrentEffect.Start();
            startButton.Text = "Stop";
        }

        private void StopEffect()
        {
            CurrentEffect.Stop();
            startButton.Text = "Start";
        }

        private Effect CurrentEffect
        {
            get
            {
                return effects[effectsComboBox.SelectedIndex];
            }
        }

        private void effectsComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            effects.ForEach(ef =>
            {
                ef.Stop();
                ef.Visible = false;
            });

            startButton.Text = "Start";
            CurrentEffect.Visible = true;
        }
    }
}